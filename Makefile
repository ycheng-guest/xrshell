XRSHELLS = xrshells

RSH = rsh
#RSH = ssh

EXPECT = /usr/local/bin/expect
#EXPECT = /usr/bin/expect

BINDIR = /usr/local/bin
#BINDIR = /usr/bin

#MANDIR = /usr/local/share/man/man1
MANDIR = /usr/local/man/man1
#MANDIR = /usr/share/man/man1
#MANDIR = /usr/man/man1

all:
	( \
		cp xrshells $(XRSHELLS).$$$$ ; \
		sed -e "1s@#\!.*@#\!$(EXPECT) -f@" \
			-e "s/xrshells/$(XRSHELLS)/g" \
			-e "s/ rsh / $(RSH) /" \
			< $(XRSHELLS).$$$$ > $(XRSHELLS) ; \
		rm -f $(XRSHELLS).$$$$ ; \
		chmod a+x $(XRSHELLS) ; \
		cp xrshells.1 $(XRSHELLS).1.$$$$ ; \
		sed -e "s/xrshells/$(XRSHELLS)/g" \
			< $(XRSHELLS).1.$$$$ > $(XRSHELLS).1 ; \
		rm -f $(XRSHELLS).1.$$$$ \
	)

install:
	install $(XRSHELLS) $(BINDIR)
	install $(XRSHELLS).1 $(MANDIR)
